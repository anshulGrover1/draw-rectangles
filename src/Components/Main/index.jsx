import React, { Component } from "react";

class Main extends Component {
  state = {
    mouse: {
      x: 0,
      y: 0,
      startX: 0,
      startY: 0,
    },
    element: null,
    cords: [],
  };

  canvasRef = React.createRef();

  handleFileUpload = (e) => {
    let canvas = this.canvasRef.current;
    this.setState({
      mouse: {
        x: 0,
        y: 0,
        startX: 0,
        startY: 0,
      },
      element: null,
      cords: [],
    });
    canvas.innerHTML = "";
    var reader = new FileReader();
    reader.onload = function (event) {
      var img = new Image();
      img.onload = function () {
        canvas.style.width = img.width + "px";
        canvas.style.height = img.height + "px";

        canvas.appendChild(img);
      };
      img.src = event.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  mouseDown = (e) => {
    let { element, mouse } = this.state;
    let canvas = this.canvasRef.current;
    if (!element) {
      mouse.startX = e.clientX;
      mouse.startY = e.clientY;
      let Newelement = document.createElement("div");
      Newelement.className = "rectangle";
      Newelement.style.left = mouse.x + "px";
      Newelement.style.top = mouse.y + "px";
      canvas.appendChild(Newelement);
      canvas.style.cursor = "crosshair";
      console.log(mouse, Newelement);
      this.setState({ mouse, element: Newelement });
    }
  };
  onMouseMove = (e) => {
    this.setMousePosition(e);
    let { mouse, element } = this.state;
    if (element) {
      element.style.width = Math.abs(mouse.x - mouse.startX) + "px";
      element.style.height = Math.abs(mouse.y - mouse.startY) + "px";
      element.style.left =
        mouse.x - mouse.startX < 0 ? mouse.x + "px" : mouse.startX + "px";
      element.style.top =
        mouse.y - mouse.startY < 0 ? mouse.y + "px" : mouse.startY + "px";
      this.setState({ mouse, element });
    }
  };

  setMousePosition = (e) => {
    let { mouse } = this.state;
    var ev = e || window.event; //Moz || IE
    if (ev.pageX) {
      //Moz
      mouse.x = ev.pageX + window.pageXOffset;
      mouse.y = ev.pageY + window.pageYOffset;
    } else if (ev.clientX) {
      //IE
      mouse.x = ev.clientX + document.body.scrollLeft;
      mouse.y = ev.clientY + document.body.scrollTop;
    }
    this.setState({ mouse });
  };
  mouseUp = (e) => {
    let { mouse, cords } = this.state;
    let cord = {
      width: mouse.x,
      height: mouse.y,
      x: mouse.startX,
      y: mouse.startY,
    };
    let newCords = [...cords, cord];
    this.setState({
      element: null,
      mouse: {
        x: 0,
        y: 0,
        startX: 0,
        startY: 0,
      },
      cords: newCords,
    });
  };
  extractCords = () => {};
  render() {
    return (
      <div className="parent">
        <div
          onMouseDown={this.mouseDown}
          onMouseUp={this.mouseUp}
          onMouseMove={this.onMouseMove}
          ref={this.canvasRef}
        ></div>
        {this.state.cords.length === 0 ? (
          <h4>
            Coordinates will appear once image is selected and recatangles are
            drawn
          </h4>
        ) : (
          <h4>Cordinates</h4>
        )}
        <ul>
          {this.state.cords.map((c) => (
            <li>
              <p>{`X:${c.x}`}</p>
              <p>{`Y:${c.y}`}</p>
              <p>{`Width:${c.width}`}</p>
              <p>{`Height:${c.height}`}</p>
            </li>
          ))}
        </ul>
        <input
          onChange={this.handleFileUpload}
          type="file"
          name="file"
          id="file"
          class="inputfile"
        />
        <label for="file">Add File</label>
      </div>
    );
  }
}

export default Main;
